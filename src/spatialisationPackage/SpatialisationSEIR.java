package spatialisationPackage;

import java.util.ArrayList;

public class SpatialisationSEIR{

	// attributs :
        private GroupePersonneSEIR gpPersonne = new GroupePersonneSEIR();

	
	public void createSpatialisation(int M, int N, double nbS, double nbE, double nbI, double nbR, double beta, double alpha, double gamma) {   

                for(int i=(int) nbS;i>0;i--)
                        { 
                            gpPersonne.add(new PersonneSEIR(0,beta, alpha, gamma, M, N));
                        }
                for(int i=(int) nbE;i>0;i--)
                        {
                            gpPersonne.add(new PersonneSEIR(1, beta, alpha, gamma, M, N));
                        }
                for(int i=(int) nbI;i>0;i--)
                        {
                            gpPersonne.add(new PersonneSEIR(2, beta, alpha, gamma, M, N));
                        }
                for(int i=(int) nbR;i>0;i--)
                        {
                            gpPersonne.add(new PersonneSEIR(3,beta, alpha, gamma, M, N));
                        }	
	}
        public void passerTempsDeplacement(){
            PersonneSEIR personne;
            int i=0;
              while(i<gpPersonne.getnbPersonne()+1)
              {
                  personne = gpPersonne.getPersonne(0);
                  personne.deplacement();
                  gpPersonne.remove(0);
                  gpPersonne.add(personne);
                  i++;
              }
        }
        
        public void passerTempsEtat(){
            PersonneSEIR personne;
            int tmpPosM,tmpPosMtest;
            int tmpPosN,tmpPosNtest;
            int i=0;
              while(i<gpPersonne.getnbPersonne()+1)
              {
                  personne = gpPersonne.getPersonne(0);
                  tmpPosM=personne.getPositionM();
                  tmpPosN=personne.getPositionN();
                  gpPersonne.remove(0);
                  gpPersonne.add(personne);
                  for(int j=(gpPersonne.getnbPersonne()-1);j>0;j--){
                      personne = gpPersonne.getPersonne(j);
                      tmpPosMtest=personne.getPositionM();
                      tmpPosNtest=personne.getPositionN();
                      if((tmpPosM==tmpPosMtest)&&(tmpPosN==tmpPosNtest))
                      {
                          personne.changementEtatSIR();
                      }
                  }
                  i++;
              }
        }
        
        public int countS()
        {
                int countS=0;
                PersonneSEIR personne;
                for(int i=0;i<gpPersonne.getnbPersonne();i++)
                {
                    personne = gpPersonne.getPersonne(i);
                    if(personne.getEtat()==0)
                    {
                        countS=countS+1;
                    }
                }        
            return countS;
        }
        
        public int countE()
        {
                int countE=0;
                PersonneSEIR personne;
                for(int i=0;i<gpPersonne.getnbPersonne();i++)
                {
                    personne = gpPersonne.getPersonne(i);
                    if(personne.getEtat()==1)
                    {
                        countE=countE+1;
                    }
                }        
            return countE;
        }
        
        public int countI()
        {
                int countI=0;
                PersonneSEIR personne;

                for(int i=0;i<gpPersonne.getnbPersonne();i++)
                {
                    personne = gpPersonne.getPersonne(i);
                    if(personne.getEtat()==2)
                    {
                        countI=countI+1;
                    }

                }        
            return countI;
        }
        
        public int countR()
        {
                int countR=0;
                PersonneSEIR personne;
                for(int i=0;i<gpPersonne.getnbPersonne();i++)
                {
                    personne = gpPersonne.getPersonne(i);
                    if(personne.getEtat()==3)
                    {
                        countR=countR+1;
                    }
                    
                }        
            return countR;
        }
}


