/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spatialisationPackage;



import java.util.ArrayList;

public class GroupePersonneSEIRevo {
		private ArrayList<PersonneSEIRevo> GroupePersonne = new ArrayList<>();
	
        public PersonneSEIRevo getPersonne(int index) {
                PersonneSEIRevo personne;
                personne = GroupePersonne.get(index);
                return personne;
        }

        public int getnbPersonne() {
                return GroupePersonne.size()-1;	 
        }

        public void add(PersonneSEIRevo personne) {
                GroupePersonne.add(personne);
        }

        public void remove(int index)
        {
                GroupePersonne.remove(index);
        }
        
        
}

