/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spatialisationPackage;



import java.util.ArrayList;

public class GroupePersonneSIR {
		private ArrayList<PersonneSIR> GroupePersonne = new ArrayList<>();
	
        public PersonneSIR getPersonne(int index) {
                PersonneSIR personne;
                personne = GroupePersonne.get(index);
                return personne;
        }

        public int getnbPersonne() {
                return GroupePersonne.size()-1;	 
        }

        public void add(PersonneSIR personne) {
                GroupePersonne.add(personne);
        }

        public void remove(int index)
        {
                GroupePersonne.remove(index);
        }
        
        
}

