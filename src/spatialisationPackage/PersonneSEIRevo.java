package spatialisationPackage;

import java.util.ArrayList;

public class PersonneSEIRevo {
	
	private int etat;
        private int m;
        private int n;
        private double beta;
        private double alpha;
        private double gamma;
        private double eta;
        private double mu;
        private int nMax;
        private int mMax;
        public final static int SAINE = 0;
	public final static int EXPOSEE = 1;
	public final static int INFECTEE = 2;
	public final static int RETIREE = 3;
	private int positionM;
	private int positionN;

	public PersonneSEIRevo(int etat, double beta, double alpha, double gamma, double eta, double mu, int nMax, int mMax) {
		this.etat = etat;
		this.mMax = mMax;
		this.nMax = nMax;
                this.beta=beta;
                this.alpha=alpha;
                this.gamma=gamma;
                this.eta=eta;
                this.mu=mu;
		this.positionM = (int) (Math.random() * mMax);
		this.positionN = (int) (Math.random() * nMax);
	}
        
        public int getEtat() {
		return etat;
	}
	
	public void setEtat(int etat) {
		this.etat = etat;
	}
	
	public int getPositionM() {
		return positionM;
	}
        
        public void setPositionM(int position) {
		this.positionM = position;
	}
	
	public int getPositionN() {
		return positionN;
	}
        	
	public void setPositionN(int position) {
		positionN = position;
	}
        
        public void deplacement() {
		positionM += (int) (Math.random() * 3) - 1;
		if (positionM >= mMax)
			positionM = 0;
		else if (positionM < 0)
			positionM = mMax - 1;
		positionN += (int) (Math.random() * 3) - 1;
		if (positionN >= nMax)
			positionN = 0;
		else if (positionN < 0)
			positionN = nMax - 1;
	}
        
        public void changementEtatSIR()
        {
            if(etat==0)
            {
                if(Math.random()<=beta)
                {
                    etat=1;
                }
            }
            if(etat==1)
            {
                if(Math.random()<=alpha)
                {
                    etat=2;
                }
            }
            else if(etat==2)
                {
                    if(Math.random()<=gamma)
                    {
                        etat=3;
                    }
                }
        }
        public int Naissance()
        {
            int value=0;
            if(Math.random()<=eta)
            {
                value=1;
            }
            return value;
        }
        
        public int Mort()
        {
            int value=0;
            if(Math.random()<=mu)
            {
                value=1;
            }
            return value;
        }
        
       
        
}
