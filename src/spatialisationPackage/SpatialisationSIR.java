package spatialisationPackage;

import java.util.ArrayList;

public class SpatialisationSIR{

	// attributs :
        private GroupePersonneSIR gpPersonne = new GroupePersonneSIR();

	
	public void createSpatialisation(int M, int N, double nbS, double nbI, double nbR, double beta, double gamma) {   

                for(int i=(int) nbS;i>0;i--)
                        {
                            gpPersonne.add(new PersonneSIR(0, beta, gamma, M, N));
                        }
                for(int i=(int) nbI;i>0;i--)
                        {
                            gpPersonne.add(new PersonneSIR(2, beta, gamma, M, N));
                        }
                for(int i=(int) nbR;i>0;i--)
                        {
                            gpPersonne.add(new PersonneSIR(3, beta, gamma, M, N));
                        }	
	}
        public void passerTempsDeplacement(){
            PersonneSIR personne;
            int i=0;
              while(i<gpPersonne.getnbPersonne()+1)
              {
                  personne = gpPersonne.getPersonne(0);
                  personne.deplacement();
                  gpPersonne.remove(0);
                  gpPersonne.add(personne);
                  i++;
              }
        }
        
        public void passerTempsEtat(){
            PersonneSIR personne;
            int tmpPosM,tmpPosMtest;
            int tmpPosN,tmpPosNtest;
            int i=0;
              while(i<gpPersonne.getnbPersonne()+1)
              {
                  personne = gpPersonne.getPersonne(0);
                  tmpPosM=personne.getPositionM();
                  tmpPosN=personne.getPositionN();
                  gpPersonne.remove(0);
                  gpPersonne.add(personne);
                  for(int j=gpPersonne.getnbPersonne()-1;j>=0;j--){
                      personne = gpPersonne.getPersonne(j);
                      tmpPosMtest=personne.getPositionM();
                      tmpPosNtest=personne.getPositionN();
                      if((tmpPosM==tmpPosMtest)&&(tmpPosN==tmpPosNtest))
                      {
                          personne.changementEtatSIR();
                      }
                  }
                  i++;
              }
        }
        
        public int countS()
        {
                int countS=0;
                PersonneSIR personne;
                for(int i=0;i<gpPersonne.getnbPersonne();i++)
                {
                    personne = gpPersonne.getPersonne(i);
                    if(personne.getEtat()==0)
                    {
                        countS=countS+1;
                    }
                }        
            return countS;
        }
        
        public int countI()
        {
                int countI=0;
                PersonneSIR personne;

                for(int i=0;i<gpPersonne.getnbPersonne();i++)
                {
                    personne = gpPersonne.getPersonne(i);
                    if(personne.getEtat()==2)
                    {
                        countI=countI+1;
                    }

                }        
            return countI;
        }
        
        public int countR()
        {
                int countR=0;
                PersonneSIR personne;
                for(int i=0;i<gpPersonne.getnbPersonne();i++)
                {
                    personne = gpPersonne.getPersonne(i);
                    if(personne.getEtat()==3)
                    {
                        countR=countR+1;
                    }
                    
                }        
            return countR;
        }
}      