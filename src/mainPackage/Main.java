package mainPackage;

import viewPackage.Vue;

/**
 * Main est la classe principale du projet.
 */
public class Main {

	/**
	 * La m�thode main cr�e une instance de la classe Vue.
	 */
	public static void main(String[] args) {
		
		new Vue();
		
	}

}
