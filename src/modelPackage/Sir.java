package modelPackage;

public class Sir {


	// attributs :

	protected double s0, i0, r0, beta, gamma;
	protected int total;


	// constructeur :

	public Sir(double s0, double i0, double r0, double beta, double gamma) {
		this.s0 = s0;
		this.i0 = i0;
		this.r0 = r0;
		this.beta = beta;
		this.gamma = gamma;
		total = (int) (s0 + i0 + r0);
	}



	// m�thodes :

	public void passerTemps() {
		// 0 repr�sente n et 1 repr�sente n+1
		double s1 = s0 - beta*s0*i0;
		double i1 = i0 + (beta*s0*i0 - gamma*i0);
		double r1 = r0 + gamma*i0;
		s0 = s1;
		i0 = i1;
		r0 = r1;
	}

	public boolean bonsParametresSir() {
		return (s0 == (int) s0) && (i0 == (int) i0) && (r0 == (int) r0) && (s0 >= 0) && (i0 >= 0) && (r0 >= 0)
				&& (beta >= 0) && (beta <= 1) && (gamma >= 0) && (gamma <= 1);
	}

	public void adapterParametresSir() {
		s0 = s0 / total;
		i0 = i0 / total;
		r0 = r0 / total;
	}
        public double getS() {
		return s0;
	}
	
	public double getI() {
		return i0;
	}
	
	public double getR() {
		return r0;
	}

	public String toString() {
		return "Sur " + total + " personnes, il y a actuellement " + (s0*100) + "% personnes saine(s), " + (i0*100) + "% personnes infect�e(s) et " + (r0*100) + "% personnes retir�e(s).";
	}

}


