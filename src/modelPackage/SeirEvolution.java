package modelPackage;

public class SeirEvolution extends Seir {

	// attributs :
	
	private double eta, mu;
	
	
	
	// constructeurs :
	
	public SeirEvolution(double s0, double e0, double i0, double r0, double beta, double alpha, double gamma, double eta, double mu) {
		super(s0, e0, i0, r0, beta, alpha, gamma);
		this.eta = eta;
		this.mu = mu;
		total = (int) (s0 + e0 + i0 + r0);
	}
	
	
	
	// m�thodes :
	
	public void passerTemps() {
		// 0 repr�sente n et 1 repr�sente n+1
		double n0 = s0 + e0 + i0 + r0;
		double s1 = s0 + (-beta*s0*i0 + eta*n0 - mu*s0);
		double e1 = e0 + (beta*s0*i0 - alpha*e0 - mu*e0);
		double i1 = i0 + (alpha*e0 - gamma*i0 - mu*i0);
		double r1 = r0 + (gamma*i0 - mu*r0);
		s0 = s1;
		e0 = e1;
		i0 = i1;
		r0 = r1;
	}
	
	public boolean bonsParametresSeirEvolution() {
		return (this.bonsParametresSeir()) && (eta >= 0) && (eta <= 1) && (mu >= 0) && (mu <= 1);
                
	}
	public double getS() {
        return s0;
        }

        public double getI() {
                return i0;
        }

        public double getR() {
                return r0;
        }

        public double getE() {
                return e0;
        }

        public String toString() {
                return "Sur " + total + " personnes, il y a actuellement " + (s0*100) + "% personnes saine(s), " + (e0*100) + "% personnes expos�e(s), " + (i0*100) + "% personnes infect�e(s) et " + (r0*100) + "% personnes retir�e(s).";
        }
}
