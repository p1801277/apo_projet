package viewPackage;

import java.awt.*;
import java.awt.event.ItemEvent;
import javax.swing.*;

public class ChoixPolitiques extends Choix {

	// attributs :
	
	private JTextField tfConfinement;
	private JTextField tfMasque;
	private JTextField tfQuarantaine;
	private JTextField tfVaccination;
	public final static String PAS_DE_POLITIQUES = "Ne pas mettre en place de politiques publiques";
	public final static String POLITIQUES = "Mettre en place des politiques publiques pour freiner l'�pid�mie";
	private String comboBoxItems[] = { PAS_DE_POLITIQUES, POLITIQUES };
	private JPanel panelComboBox;
    private JComboBox<String> comboBox;
    private JPanel cards;
    private JPanel card1;
    private JPanel card2;
	
    
    
    // constructeur :
    
    public ChoixPolitiques() {
    	panelComboBox = new JPanel();
    	comboBox = new JComboBox<String>(comboBoxItems);
    	comboBox.setEditable(false);
    	comboBox.addItemListener(this);
        panelComboBox.add(comboBox);
        
        card1 = new JPanel();
        JLabel labelPasDePolitiques = new JLabel("Pas de politiques publiques.");
        labelPasDePolitiques.setFont(new Font("Dialog", Font.ITALIC, 12));
        card1.add(labelPasDePolitiques);
        
        card2 = new JPanel(new GridLayout(2, 4, 10, 0));
        JLabel labelConfinement = new JLabel("Confinement");
        card2.add(labelConfinement);
        JLabel labelMasque = new JLabel("Port du masque");
        card2.add(labelMasque);
        JLabel labelQuarantaine = new JLabel("Quarantaine");
        card2.add(labelQuarantaine);
        JLabel labelVaccination = new JLabel("Vaccination");
        card2.add(labelVaccination);
        tfConfinement = new JTextField();
        card2.add(tfConfinement);
        tfMasque = new JTextField();
        card2.add(tfMasque);
        tfQuarantaine = new JTextField();
        card2.add(tfQuarantaine);
        tfVaccination = new JTextField();
        card2.add(tfVaccination);
        
        cards = new JPanel(new CardLayout());
        cards.add(card1, PAS_DE_POLITIQUES);
        cards.add(card2, POLITIQUES);
    }

    
    
    // m�thodes :
    
	public void reinitialiser() {
		tfConfinement.setText("");
		tfMasque.setText("");
		tfQuarantaine.setText("");
		tfVaccination.setText("");
		comboBox.setSelectedItem(PAS_DE_POLITIQUES);
	}
	
	public Object getSelection() {
    	return comboBox.getSelectedItem();
    }
    
	public String[] getPolitiquesValues() {
		String[] tab = new String[4];
    	tab[0] = tfConfinement.getText();
    	tab[1] = tfMasque.getText();
    	tab[2] = tfQuarantaine.getText();
    	tab[3] = tfVaccination.getText();
    	return tab;
	}
	
    public void addComponentToPane(Container c) {
        c.add(panelComboBox, BorderLayout.PAGE_START);
        c.add(cards, BorderLayout.CENTER);
    }
    
	public void itemStateChanged(ItemEvent evt) {
		CardLayout cl = (CardLayout)(cards.getLayout());
        cl.show(cards, (String)evt.getItem());
	}
	
}
