package viewPackage;

import java.awt.*;
import java.awt.event.ItemEvent;
import javax.swing.*;

public class ChoixSpatialisation extends Choix {

	// attributs :
	
	private JTextField tfLongueur;
	private JTextField tfLargeur;
	public final static String PAS_DE_SPATIALISATION = "Ne pas utiliser la spatialisation";
	public final static String SPATIALISATION = "Utiliser la spacialisation";
	private String comboBoxItems[] = { PAS_DE_SPATIALISATION, SPATIALISATION };
	private JPanel panelComboBox;
    private JComboBox<String> comboBox;
    private JPanel cards;
    private JPanel card1;
    private JPanel card2;
    private JPanel panelSpatialisation;
	private ChoixPolitiques choixPolitiques;
	private JPanel panelChoixPolitiques;
    
    
    
    // constructeur :
    
    public ChoixSpatialisation() {
		panelComboBox = new JPanel();
		comboBox = new JComboBox<String>(comboBoxItems);
		comboBox.setEditable(false);
		comboBox.addItemListener(this);
        panelComboBox.add(comboBox);
        
        card1 = new JPanel();
        JLabel labelPasDePolitiques = new JLabel("Pas de spatialisation.");
        labelPasDePolitiques.setFont(new Font("Dialog", Font.ITALIC, 12));
        card1.add(labelPasDePolitiques);
        
        card2 = new JPanel(new GridLayout(2, 1));
        panelSpatialisation = new JPanel(new GridLayout(2, 2, 10, 0));
        JLabel labelLongueur = new JLabel("Longueur de la grille torique (M)");
        panelSpatialisation.add(labelLongueur);
        JLabel labelLargeur = new JLabel("Largeur de la grille torique (N)");
        panelSpatialisation.add(labelLargeur);
        tfLongueur = new JTextField();
        panelSpatialisation.add(tfLongueur);
        tfLargeur = new JTextField();
        panelSpatialisation.add(tfLargeur);
        card2.add(panelSpatialisation);
        //
        choixPolitiques = new ChoixPolitiques();
		panelChoixPolitiques = new JPanel(new BorderLayout());
		panelChoixPolitiques.setBorder(BorderFactory.createEmptyBorder(20, 20, 0, 20));
		choixPolitiques.addComponentToPane(panelChoixPolitiques);
		card2.add(panelChoixPolitiques);
        
        cards = new JPanel(new CardLayout());
        cards.add(card1, PAS_DE_SPATIALISATION);
        cards.add(card2, SPATIALISATION);
	}

	
	
    // m�thodes :
    
    public void reinitialiser() {
		tfLongueur.setText("");
		tfLargeur.setText("");
		choixPolitiques.reinitialiser();
		comboBox.setSelectedItem(PAS_DE_SPATIALISATION);
	}
    
    public Object getSelection() {
    	return comboBox.getSelectedItem();
    }
	
    public Object getPolitiquesSelection() {
    	return choixPolitiques.getSelection();
    }
    
    public String[] getSpatialisationValues() {
    	String[] tab = new String[2];
    	tab[0] = tfLongueur.getText();
    	tab[1] = tfLargeur.getText();
    	return tab;
    }
    
	public void addComponentToPane(Container c) {
		c.add(panelComboBox, BorderLayout.PAGE_START);
        c.add(cards, BorderLayout.CENTER);
	}
	
	public void itemStateChanged(ItemEvent evt) {
		CardLayout cl = (CardLayout)(cards.getLayout());
        cl.show(cards, (String)evt.getItem());
	}






	
	
}
