package viewPackage;

import java.awt.*;
import java.awt.event.ItemEvent;
import javax.swing.*;

public class ChoixModele extends Choix {

	// attributs :
	
	private JTextField tfS1, tfS2, tfS3;
	private JTextField tfI1, tfI2, tfI3;
	private JTextField tfE2, tfE3;
	private JTextField tfR1, tfR2, tfR3;
	private JTextField tfBeta1, tfBeta2, tfBeta3;
	private JTextField tfAlpha2, tfAlpha3;
	private JTextField tfGamma1, tfGamma2, tfGamma3;
	private JTextField tfEta3;
	private JTextField tfMu3;
    public final static String MODELE_SIR = "Mod�le SIR";
    public final static String MODELE_SEIR = "Mod�le SEIR";
    public final static String MODELE_SEIR_EVO = "Mod�le SEIR avec �volution de quantit� de population";
    private String comboBoxItems[] = { MODELE_SIR, MODELE_SEIR, MODELE_SEIR_EVO };
	private JPanel panelComboBox;
    private JComboBox<String> comboBox;
    private JPanel cards;
    private JPanel card1;
    private JPanel card2;
    private JPanel card3;
    
    
    
    // constructeur :
    
    public ChoixModele() {
    	panelComboBox = new JPanel();
    	comboBox = new JComboBox<String>(comboBoxItems);
    	comboBox.setEditable(false);
    	comboBox.addItemListener(this);
        panelComboBox.add(comboBox);
         
        card1 = new JPanel(new GridLayout(2, 5, 10, 0));
        JLabel labelS1 = new JLabel("Nombre de personnes saines (S0)");
        card1.add(labelS1);
        JLabel labelI1 = new JLabel("Nombre de personnes infect�es (I0)");
        card1.add(labelI1);
        JLabel labelR1 = new JLabel("Nombre de personnes retir�es (R0)");
        card1.add(labelR1);
        JLabel labelBeta1 = new JLabel("Probabilit� d'infection (Beta)");
        card1.add(labelBeta1);
        JLabel labelGamma1 = new JLabel("Probabilit� de retirement (Gamma)");
        card1.add(labelGamma1); 
        tfS1 = new JTextField();
        card1.add(tfS1);
        tfI1 = new JTextField();
        card1.add(tfI1);
        tfR1 = new JTextField();
        card1.add(tfR1);
        tfBeta1 = new JTextField();
        card1.add(tfBeta1);
        tfGamma1 = new JTextField();
        card1.add(tfGamma1);
        
        card2 = new JPanel(new GridLayout(2, 7, 10, 0));
        JLabel labelS2 = new JLabel("<html>Nombre de personnes<br>saines (S0)</html>");
        card2.add(labelS2);
        JLabel labelE2 = new JLabel("<html>Nombre de personnes<br>expos�es (E0)</html>");
        card2.add(labelE2);
        JLabel labelI2 = new JLabel("<html>Nombre de personnes<br>infect�es (I0)</html>");
        card2.add(labelI2);
        JLabel labelR2 = new JLabel("<html>Nombre de personnes<br>retir�es (R0)</html>");
        card2.add(labelR2);
        JLabel labelBeta2 = new JLabel("<html>Probabilit�<br>d'exposition (Beta)</html>");
        card2.add(labelBeta2);
        JLabel labelAlpha2 = new JLabel("<html>Probabilit�<br>d'infection (Alpha)</html>");
        card2.add(labelAlpha2);
        JLabel labelGamma2 = new JLabel("<html>Probabilit� de<br>retirement (Gamma)</html>");
        card2.add(labelGamma2); 
        tfS2 = new JTextField();
        card2.add(tfS2);
        tfE2 = new JTextField();
        card2.add(tfE2);
        tfI2 = new JTextField();
        card2.add(tfI2);
        tfR2 = new JTextField();
        card2.add(tfR2);
        tfBeta2 = new JTextField();
        card2.add(tfBeta2);
        tfAlpha2 = new JTextField();
        card2.add(tfAlpha2);
        tfGamma2 = new JTextField();
        card2.add(tfGamma2);
        
        card3 = new JPanel(new GridLayout(2, 9, 10, 0));
        JLabel labelS3 = new JLabel("<html>Nombre de personnes<br>saines (S0)</html>");
        card3.add(labelS3);
        JLabel labelE3 = new JLabel("<html>Nombre de personnes<br>expos�es (E0)</html>");
        card3.add(labelE3);
        JLabel labelI3 = new JLabel("<html>Nombre de personnes<br>infect�es (I0)</html>");
        card3.add(labelI3);
        JLabel labelR3 = new JLabel("<html>Nombre de personnes<br>retir�es (R0)</html>");
        card3.add(labelR3);
        JLabel labelBeta3 = new JLabel("<html>Probabilit�<br>d'exposition (Beta)</html>");
        card3.add(labelBeta3);
        JLabel labelAlpha3 = new JLabel("<html>Probabilit�<br>d'infection (Alpha)</html>");
        card3.add(labelAlpha3);
        JLabel labelGamma3 = new JLabel("<html>Probabilit� de<br>retirement (Gamma)</html>");
        card3.add(labelGamma3);
        JLabel labelEta3 = new JLabel("<html>Probabilit� de<br>naissance (Eta)</html>");
        card3.add(labelEta3);
        JLabel labelMu3 = new JLabel("<html>Probabilit� de<br>mort (Mu)</html>");
        card3.add(labelMu3);
        tfS3 = new JTextField();
        card3.add(tfS3);
        tfE3 = new JTextField();
        card3.add(tfE3);
        tfI3 = new JTextField();
        card3.add(tfI3);
        tfR3 = new JTextField();
        card3.add(tfR3);
        tfBeta3 = new JTextField();
        card3.add(tfBeta3);
        tfAlpha3 = new JTextField();
        card3.add(tfAlpha3);
        tfGamma3 = new JTextField();
        card3.add(tfGamma3);
        tfEta3 = new JTextField();
        card3.add(tfEta3);
        tfMu3 = new JTextField();
        card3.add(tfMu3);
        
        cards = new JPanel(new CardLayout());
        cards.add(card1, MODELE_SIR);
        cards.add(card2, MODELE_SEIR);
        cards.add(card3, MODELE_SEIR_EVO);
    }
    
    
    
    // m�thodes :
    
    public void reinitialiser() {
    	tfS1.setText("");
    	tfS2.setText("");
    	tfS3.setText("");
    	tfI1.setText("");
    	tfI2.setText("");
    	tfI3.setText("");
    	tfE2.setText("");
    	tfE3.setText("");
    	tfR1.setText("");
    	tfR2.setText("");
    	tfR3.setText("");
    	tfBeta1.setText("");
    	tfBeta2.setText("");
    	tfBeta3.setText("");
    	tfAlpha2.setText("");
    	tfAlpha3.setText("");
    	tfGamma1.setText("");
    	tfGamma2.setText("");
    	tfGamma3.setText("");
    	tfEta3.setText("");
    	tfMu3.setText("");
    	comboBox.setSelectedItem(MODELE_SIR);
    }
    
    public Object getSelection() {
    	return comboBox.getSelectedItem();
    }
    
    public String[] getSirValues() {
    	String[] tab = new String[5];
    	tab[0] = tfS1.getText();
    	tab[1] = tfI1.getText();
    	tab[2] = tfR1.getText();
    	tab[3] = tfBeta1.getText();
    	tab[4] = tfGamma1.getText();
    	return tab;
    }
    
    public String[] getSeirValues() {
    	String[] tab = new String[7];
    	tab[0] = tfS2.getText();
    	tab[1] = tfE2.getText();
    	tab[2] = tfI2.getText();
    	tab[3] = tfR2.getText();
    	tab[4] = tfBeta2.getText();
    	tab[5] = tfAlpha2.getText();
    	tab[6] = tfGamma2.getText();
    	return tab;
    }
    
    public String[] getSeirEvoValues() {
    	String[] tab = new String[9];
    	tab[0] = tfS3.getText();
    	tab[1] = tfE3.getText();
    	tab[2] = tfI3.getText();
    	tab[3] = tfR3.getText();
    	tab[4] = tfBeta3.getText();
    	tab[5] = tfAlpha3.getText();
    	tab[6] = tfGamma3.getText();
    	tab[7] = tfEta3.getText();
    	tab[8] = tfMu3.getText();
    	return tab;
    }
    
    public void addComponentToPane(Container c) {
        c.add(panelComboBox, BorderLayout.PAGE_START);
        c.add(cards, BorderLayout.CENTER);
    }

    public void itemStateChanged(ItemEvent evt) {
        CardLayout cl = (CardLayout)(cards.getLayout());
        cl.show(cards, (String)evt.getItem());
    }

}
