package viewPackage;

import spatialisationPackage.SpatialisationSIR;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import modelPackage.*;
import spatialisationPackage.*;


public class Vue implements ActionListener {
	
	// attributs :
	private final static int NBITERATIONS = 1000; //nb itération, utile pour régler l'échelle (abscisse) 
        
	private JFrame frame;
	private JPanel panelConsigne;
	private JLabel labelConsigne;
	private JPanel panelChoixModele;
	private ChoixModele choixModele;
	private JPanel panelChoixSpatialisation;
	private ChoixSpatialisation choixSpatialisation;
	private JPanel panelBoutons;
	private JButton bLancer;
	private JButton bReinitialiser;
	private JButton bQuitter;
	private JDialog dialog;
	private JPanel panelDialog;
	private JLabel labelDialog;
	private JPanel panelCentre;
        private plotSIR chartSIR;
        private plotSEIR chartSEIR;
        private plotSEIR chartSEIRevo;
	private plotSIR chartspaceSIR;
	private plotSEIR chartspaceSEIR;
        private plotSEIR chartspaceSEIRevo;
        private SpatialisationSIR spaceSIR;
        private SpatialisationSEIR spaceSEIR;
        private SpatialisationSEIRevo spaceSEIRevo;
        
	
	
	// constructeur :
	
	public Vue() {
		frame = new JFrame("CardLayoutDemo");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.setTitle("Projet APO");
		frame.setResizable(false);
		
        labelConsigne = new JLabel("Veuillez choisir un mod�le et ses param�tres :");
        labelConsigne.setFont(new Font("Dialog", Font.BOLD, 20));
        panelConsigne = new JPanel();
        panelConsigne.setBorder(BorderFactory.createEmptyBorder(10, 100, 0, 100));
        panelConsigne.add(labelConsigne);
        frame.add(panelConsigne, BorderLayout.NORTH);
        
        panelCentre = new JPanel(new GridLayout(2, 1, 0, 20));
        
		choixModele = new ChoixModele();
		panelChoixModele = new JPanel(new BorderLayout());
		panelChoixModele.setBorder(BorderFactory.createEmptyBorder(0, 15, 0, 15));
		choixModele.addComponentToPane(panelChoixModele);
		panelCentre.add(panelChoixModele);
        
		choixSpatialisation = new ChoixSpatialisation();
		panelChoixSpatialisation = new JPanel(new BorderLayout());
		panelChoixSpatialisation.setBorder(BorderFactory.createEmptyBorder(0, 150, 0, 150));
		choixSpatialisation.addComponentToPane(panelChoixSpatialisation);
		panelCentre.add(panelChoixSpatialisation);
		
		panelCentre.setBorder(BorderFactory.createEmptyBorder(30, 0, 30, 0));
		frame.add(panelCentre, BorderLayout.CENTER);
		
		bLancer = new JButton("Lancer la simulation");
		bLancer.addActionListener(this);
		bReinitialiser = new JButton("R�initialiser");
		bReinitialiser.addActionListener(this);
		bQuitter = new JButton("Quitter");
		bQuitter.addActionListener(this);
		panelBoutons = new JPanel(new GridLayout(1, 3, 30, 0));
		panelBoutons.setBorder(BorderFactory.createEmptyBorder(15, 100, 15, 100));
		panelBoutons.add(bLancer);
		panelBoutons.add(bReinitialiser);
		panelBoutons.add(bQuitter);
		frame.add(panelBoutons, BorderLayout.SOUTH);
		
		panelDialog = new JPanel();
		panelDialog.setBorder(BorderFactory.createEmptyBorder(10, 250, 40, 250));
		labelDialog = new JLabel();
		labelDialog.setForeground(Color.RED);
		panelDialog.add(labelDialog);
		dialog = new JDialog(frame, "Erreur");
		dialog.add(panelDialog);
		dialog.pack();
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setVisible(false);
		
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
	
	
	
	// m�thodes :
	
	public void actionPerformed(ActionEvent ae) {
		dialog.dispose();
		dialog.setLocationRelativeTo(frame);
		if (ae.getSource() == bLancer) {
			try {
				if (choixModele.getSelection() == ChoixModele.MODELE_SIR) {
					if (choixSpatialisation.getSelection() == ChoixSpatialisation.PAS_DE_SPATIALISATION) {
						
						String[] tabString = choixModele.getSirValues();
						double[] tabDouble = new double[5];
						for (int i = 0; i < tabString.length; i++) {
							tabDouble[i] = Double.parseDouble(tabString[i]);
						}
						Sir sir = new Sir(tabDouble[0], tabDouble[1], tabDouble[2], tabDouble[3], tabDouble[4]);
						
						if (sir.bonsParametresSir()) {
							sir.adapterParametresSir();
                                                        
                                                chartSIR = new plotSIR(
                                                "line chart" ,
                                                "Evolutions des catégories de personne en fct du temps (SIR)",
                                                NBITERATIONS);
							// affichage
                                                for(int i=0;i<NBITERATIONS;i++) {
                                                    chartSIR.getValueS(sir.getS(),i);
                                                    chartSIR.getValueI(sir.getI(),i);
                                                    chartSIR.getValueR(sir.getR(),i);
                                                    sir.passerTemps();
                                                }
                                                    chartSIR.plotChart(chartSIR);


						} else {
							labelDialog.setText("<html>Veuillez entrer un entier sup�rieur ou �gal � 0 pour les nombres S0, E0, I0, et R0.<br>Veuillez entrer un r�el compris entre 0 et 1 (inclus) pour les probabilit�s.</html>");
							dialog.setVisible(true);
						}
					} else { 
                                                String[] tabString = choixModele.getSirValues();
						double[] tabDouble = new double[5];
						for (int i = 0; i < tabString.length; i++) {
							tabDouble[i] = Double.parseDouble(tabString[i]);
						}
                                                String[] tabStringSpace = choixSpatialisation.getSpatialisationValues();
                                                int[] tabDoubleSpace = new int[2];
                                                for (int i = 0; i < tabStringSpace.length; i++) {
							tabDoubleSpace[i] = Integer.parseInt(tabStringSpace[i]);
						}
                                                spaceSIR = new SpatialisationSIR();
                                                spaceSIR.createSpatialisation(tabDoubleSpace[0],tabDoubleSpace[1], tabDouble[0],tabDouble[1], tabDouble[2], tabDouble[3], tabDouble[4]);
                                                chartspaceSIR = new plotSIR(
                                                "line chart" ,
                                                "Evolutions des catégories de personne en fct du temps (SPATIALISATION SIR)",
                                                NBITERATIONS);
                                                for(int i=0;i<NBITERATIONS;i++) {
                                                        chartspaceSIR.getValueS(spaceSIR.countS(),i);
                                                        chartspaceSIR.getValueI(spaceSIR.countI(),i);
                                                        chartspaceSIR.getValueR(spaceSIR.countR(),i);


                                                    spaceSIR.passerTempsDeplacement();
                                                    spaceSIR.passerTempsEtat();
                                                }
                                                    chartspaceSIR.plotChart(chartspaceSIR);
						if (choixSpatialisation.getPolitiquesSelection() == ChoixPolitiques.POLITIQUES) {
							
							//
							
						}
						
						//

					}
				} else if (choixModele.getSelection() == ChoixModele.MODELE_SEIR) {
					if (choixSpatialisation.getSelection() == ChoixSpatialisation.PAS_DE_SPATIALISATION) {
						
						String[] tabString = choixModele.getSeirValues();
						double[] tabDouble = new double[7];
						for (int i = 0; i < tabString.length; i++) {
							tabDouble[i] = Double.parseDouble(tabString[i]);
						}
						Seir seir = new Seir(tabDouble[0], tabDouble[1], tabDouble[2], tabDouble[3], tabDouble[4], tabDouble[5], tabDouble[6]);
						
						if (seir.bonsParametresSeir()) {
							seir.adapterParametresSeir();
                                                        chartSEIR = new plotSEIR(
                                                        "line chart" ,
                                                        "Evolutions des catégories de personne en fct du temps (SEIR)",
                                                        NBITERATIONS);  //nb itérations
							// affichage
                                                        for(int i=0;i<NBITERATIONS;i++) {
                                                            chartSEIR.getValueS(seir.getS(),i);
                                                            chartSEIR.getValueE(seir.getE(),i);
                                                            chartSEIR.getValueI(seir.getI(),i);
                                                            chartSEIR.getValueR(seir.getR(),i);
                                                            
                                                            seir.passerTemps();
                                                        }
                                                        chartSEIR.plotChart(chartSEIR);
							
						} else {
							labelDialog.setText("<html>Veuillez entrer un entier sup�rieur ou �gal � 0 pour les nombres S0, E0, I0, et R0.<br>Veuillez entrer un r�el compris entre 0 et 1 (inclus) pour les probabilit�s.</html>");
							dialog.setVisible(true);
						}
						
					} else {
                                                String[] tabString = choixModele.getSeirValues();
						double[] tabDouble1 = new double[7];
						for (int i = 0; i < tabString.length; i++) {
							tabDouble1[i] = Double.parseDouble(tabString[i]);
						}
                                                String[] tabStringSpace = choixSpatialisation.getSpatialisationValues();
                                                double[] tabDoubleSpace = new double[2];
                                                for (int i = 0; i < tabStringSpace.length; i++) {
							tabDoubleSpace[i] = Double.parseDouble(tabStringSpace[i]);
						}
                                                spaceSEIR = new SpatialisationSEIR();
                                                spaceSEIR.createSpatialisation((int) tabDoubleSpace[0], (int) tabDoubleSpace[1], tabDouble1[0],tabDouble1[1], tabDouble1[2], tabDouble1[3], tabDouble1[4],tabDouble1[5],tabDouble1[6]);
                                                chartspaceSEIR = new plotSEIR(
                                                "line chart" ,
                                                "Evolutions des catégories de personne en fct du temps en jour (SPATIALISATION SEIR)",
                                                NBITERATIONS);
                                                for(int i=0;i<NBITERATIONS;i++) {
                                                
                                                    chartspaceSEIR.getValueS(spaceSEIR.countS(),i);
                                                    chartspaceSEIR.getValueE(spaceSEIR.countE(),i);
                                                    chartspaceSEIR.getValueI(spaceSEIR.countI(),i);
                                                    chartspaceSEIR.getValueR(spaceSEIR.countR(),i);


                                                    spaceSEIR.passerTempsDeplacement();
                                                    spaceSEIR.passerTempsEtat();
                                                }
                                                    chartspaceSEIR.plotChart(chartspaceSEIR);
						if (choixSpatialisation.getPolitiquesSelection() == ChoixPolitiques.POLITIQUES) {
						
							//
							
						}
						
						//

					}
				} else {
					if (choixSpatialisation.getSelection() == ChoixSpatialisation.PAS_DE_SPATIALISATION) {
						
						String[] tabString = choixModele.getSeirEvoValues();
						double[] tabDouble = new double[9];
						for (int i = 0; i < tabString.length; i++) {
							tabDouble[i] = Double.parseDouble(tabString[i]);
						}
						SeirEvolution seirEvo = new SeirEvolution(tabDouble[0], tabDouble[1], tabDouble[2], tabDouble[3], tabDouble[4], tabDouble[5], tabDouble[6], tabDouble[7], tabDouble[8]);
						
						if (seirEvo.bonsParametresSeirEvolution()) {
							seirEvo.adapterParametresSeir();
                                                        chartSEIRevo = new plotSEIR(
                                                        "line chart" ,
                                                        "Evolutions des catégories de personne en fct du temps en jour (SEIRevo)",
                                                        NBITERATIONS);  //nb itérations
							// affichage
                                                        for(int i=0;i<NBITERATIONS;i++) {
                                                            chartSEIRevo.getValueS(seirEvo.getS(),i);
                                                            chartSEIRevo.getValueE(seirEvo.getE(),i);
                                                            chartSEIRevo.getValueI(seirEvo.getI(),i);
                                                            chartSEIRevo.getValueR(seirEvo.getR(),i);
                                                            
                                                            seirEvo.passerTemps();
                                                        }
                                                        chartSEIRevo.plotChart(chartSEIRevo);
							
						} else {
							labelDialog.setText("<html>Veuillez entrer un entier sup�rieur ou �gal � 0 pour les nombres S0, E0, I0, et R0.<br>Veuillez entrer un r�el compris entre 0 et 1 (inclus) pour les probabilit�s.</html>");
							dialog.setVisible(true);
						}
						
					} else {
                                                String[] tabString = choixModele.getSeirEvoValues();
						double[] tabDouble1 = new double[9];
						for (int i = 0; i < tabString.length; i++) {
							tabDouble1[i] = Double.parseDouble(tabString[i]);
						}
                                                String[] tabStringSpace = choixSpatialisation.getSpatialisationValues();
                                                double[] tabDoubleSpace = new double[2];
                                                for (int i = 0; i < tabStringSpace.length; i++) {
							tabDoubleSpace[i] = Double.parseDouble(tabStringSpace[i]);
						}
                                                spaceSEIRevo = new SpatialisationSEIRevo();
                                                spaceSEIRevo.createSpatialisation((int) tabDoubleSpace[0], (int) tabDoubleSpace[1], tabDouble1[0],tabDouble1[1], tabDouble1[2], tabDouble1[3], tabDouble1[4],tabDouble1[5],tabDouble1[6],tabDouble1[7],tabDouble1[8]);
                                                chartspaceSEIRevo = new plotSEIR(
                                                "line chart" ,
                                                "Evolutions des catégories de personne en fct du temps en jour (SPATIALISATION SEIRevo)",
                                                NBITERATIONS);
                                                for(int i=0;i<NBITERATIONS;i++) {
                                                
                                                    chartspaceSEIRevo.getValueS(spaceSEIRevo.countS(),i);
                                                    chartspaceSEIRevo.getValueE(spaceSEIRevo.countE(),i);
                                                    chartspaceSEIRevo.getValueI(spaceSEIRevo.countI(),i);
                                                    chartspaceSEIRevo.getValueR(spaceSEIRevo.countR(),i);
                                                    spaceSEIRevo.passerTempsDeplacement();
                                                    spaceSEIRevo.passerTempsEtat();
                                                    spaceSEIRevo.passerTempsNaissance();
                                                    spaceSEIRevo.passerTempsMort();
                                                }
                                                    chartspaceSEIRevo.plotChart(chartspaceSEIRevo);
						if (choixSpatialisation.getPolitiquesSelection() == ChoixPolitiques.POLITIQUES) {
							
							//
							
						}
						
						//

					}
				}
			} catch (Exception e) {
				labelDialog.setText("Veuillez entrer tous les param�tres n�cessaires.");
				dialog.setVisible(true);
			}
		} else if (ae.getSource() == bReinitialiser) {
			choixModele.reinitialiser();
			choixSpatialisation.reinitialiser();
		} else {
			frame.dispose();
		}
	}

}
