package viewPackage;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

public class plotSEIR extends ApplicationFrame {
       private double monTableauS[];
       private double monTableauE[];
       private double monTableauI[];
       private double monTableauR[];
       private DefaultCategoryDataset dataset;

   public plotSEIR( String applicationTitle , String chartTitle, int nbIterations ) {
      super(applicationTitle);
      dataset = new DefaultCategoryDataset();
      JFreeChart lineChart = ChartFactory.createLineChart(
         chartTitle,
         "Jours","Nombre de personnes par catégorie",
         createDataset(),
         PlotOrientation.VERTICAL,
         true,true,false);
         
      ChartPanel chartPanel = new ChartPanel( lineChart );
      chartPanel.setPreferredSize( new java.awt.Dimension( 560 , 367 ) );
      setContentPane( chartPanel );
      monTableauS=new double[nbIterations];
      monTableauE=new double[nbIterations];
      monTableauI=new double[nbIterations];
      monTableauR=new double[nbIterations];
      
   }
   
   public void getValueS(double value, int index)
   {
       monTableauS[index]=value;
       dataset.addValue( monTableauS[index] , "personnes saines" , String.valueOf(index));
   }
   public void getValueI(double value, int index)
   {
       monTableauI[index]=value;
       dataset.addValue( monTableauI[index] , "personnes infectées" , String.valueOf(index) );
   }
   public void getValueR(double value, int index)
   {
       monTableauR[index]=value;
       dataset.addValue( monTableauR[index] , "personnes retirées" , String.valueOf(index) );

   }
   
   public void getValueE(double value, int index)
   {
       monTableauE[index]=value;
       dataset.addValue( monTableauE[index] , "personnes exposées" , String.valueOf(index) );

   }
    


   private DefaultCategoryDataset createDataset( ) {
      for(int j=0;j<100;j++)
      {
          dataset.addValue( 0 , "personnes saines" , String.valueOf(j) );
          dataset.addValue( 0 , "personnes infectées" , String.valueOf(j) );
          dataset.addValue( 0 , "personnes retirées" , String.valueOf(j) );
          dataset.addValue( 0 , "personnes exposées" , String.valueOf(j) );
      }
      
      return dataset;
   }
   
    public void plotChart(plotSEIR classe)
    {
        classe.pack( );
        RefineryUtilities.centerFrameOnScreen( classe );
        classe.setVisible(true);
    }
}
