package viewPackage;

import java.awt.Container;
import java.awt.event.ItemListener;

public abstract class Choix implements ItemListener {
	
	// m�thodes :
	
	public abstract void reinitialiser();
	
	public abstract Object getSelection();
	
	public abstract void addComponentToPane(Container c);
	
}
