package viewPackage;

import java.util.ArrayList;
import java.util.List;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

public class plotSIR extends ApplicationFrame {
       private double monTableauS[];
       private double monTableauI[];
       private double monTableauR[];
       private int getnbIterations;
       private DefaultCategoryDataset dataset;

   public plotSIR( String applicationTitle , String chartTitle, int nbIterations ) {
      super(applicationTitle);
      dataset = new DefaultCategoryDataset();
      JFreeChart lineChart = ChartFactory.createLineChart(
         chartTitle,
         "Jours","% par catégorie",
         createDataset(),
         PlotOrientation.VERTICAL,
         true,true,false);
         
      ChartPanel chartPanel = new ChartPanel( lineChart );
      chartPanel.setPreferredSize( new java.awt.Dimension( 560 , 367 ) );
      setContentPane( chartPanel );
      monTableauS=new double[nbIterations];
      monTableauI=new double[nbIterations];
      monTableauR=new double[nbIterations];
      getnbIterations=nbIterations;

      
   }
   
   public void getValueS(double value, int index)
   {
       monTableauS[index]=value;
       dataset.addValue( monTableauS[index] , "personnes saines" , String.valueOf(index));
       
   }
   public void getValueI(double value, int index)
   {
       monTableauI[index]=value;
       dataset.addValue( monTableauI[index] , "personnes infectées" , String.valueOf(index) );
   }
   public void getValueR(double value, int index)
   {
       monTableauR[index]=value;
       dataset.addValue( monTableauR[index] , "personnes retirées" , String.valueOf(index) );
   }


   private DefaultCategoryDataset createDataset( ) {
      for(int j=0;j<100;j++)
      {
          dataset.addValue( 0 , "personnes saines" , String.valueOf(j) );
          dataset.addValue( 0 , "personnes infectées" , String.valueOf(j) );
          dataset.addValue( 0 , "personnes retirées" , String.valueOf(j) );
      }
      
      return dataset;
   }
   
    public void plotChart(plotSIR classe)
    {
        classe.pack( );
        RefineryUtilities.centerFrameOnScreen( classe );
        classe.setVisible(true);
    }
    
}

